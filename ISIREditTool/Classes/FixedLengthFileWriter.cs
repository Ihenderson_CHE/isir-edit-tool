﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;
using ISIREditTool.Classes;

namespace ISIREditTool.Classes
{
    class FixedLengthFileWriter
    {
        private readonly string _readFilePath;
        private readonly string _writeFilePath;

        private readonly List<List<ISIRCell>> ISIR;
         
        public FixedLengthFileWriter(string filepath, string folderpath, List<List<ISIRCell>> isir)
        {
            _readFilePath = filepath;
            _writeFilePath = folderpath;
            ISIR = isir;
        }

        public void Write()
        {
            string line;
            StreamReader reader = new StreamReader(_readFilePath);
            StreamWriter writer = new StreamWriter(_writeFilePath);
            var FirstRow = true;
            var row = 0;
            while ((line = reader.ReadLine()) != null)
            {
                string newLine = line;
                
                if (!FirstRow)
                {
                    for (int c = 0; c < ISIR[row-1].Count; c++)
                    {
                        string startingValue = line.Substring(ISIR[row - 1][c].Element.Start -1, ISIR[row - 1][c].Element.Length);
                        string newValue = PadElement(ISIR[row - 1][c].Value, ISIR[row - 1][c].Element.Type, ISIR[row - 1][c].Element.Justification, ISIR[row - 1][c].Element.Length);

                        if (!startingValue.Trim().Equals(newValue.Trim()))
                        {
							//Prepare value to be pushed into ISIR

							//Replace value in ISIR Row
	                        try
	                        {
		                        newLine = newLine.Remove(ISIR[row - 1][c].Element.Start - 1, ISIR[row - 1][c].Element.Length);

	                        }
	                        catch (Exception)
	                        {
	                        }
	                        newLine = newLine.Insert(ISIR[row - 1][c].Element.Start - 1, newValue);
                        }
                    }
                }
                else
                {
                    FirstRow = false;
                }
                //Write line to file
                writer.WriteLine(newLine);
                row++;
            }
            writer.Close();
        }

        private  string PadElement(string value, string type, string justification,int length)
        {
            string padValue = string.Empty;
          
            switch (type)
            {
                case "string":
                    if (justification == "right")
                    {
                        return value.Trim().PadLeft(length);
                    }
                    else
                    {
                        return value.PadRight(length);
                    }
                   
                case "numeric":
                    return PadNumber(value, length);
                   
                case "money":
                    return PadMoney(value, length);
                   
                case "date":
                    return PadDate(value);
                   
            }

            return padValue;
        }

        private string PadNumber(string value, int length)
        {
            while (value.Length < length)
            {
                value = "0" + value;
            }
            return value;
        }

        private string PadMoney(string value, int length)
        {
            ProcessingCodes codes = new ProcessingCodes();
            value = codes.NumberToCodeConverter(value);
            for (int i = 1; i <= (length - value.Trim().Length); i++)
            {
                value = "0" + value;
            }
            return value;
        }

        private string PadDate(string value)
        {
            ProcessingCodes code = new ProcessingCodes();
            return code.DateToISIRDate(value);
        }
    }
   


   
}
