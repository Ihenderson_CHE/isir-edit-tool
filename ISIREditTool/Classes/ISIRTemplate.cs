﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Microsoft.Office.Interop.Excel;
using Telerik.WinControls.UI.Export;

namespace ISIREditTool.Classes
{
    public class ISIRTemplate
    {

        public IEnumerable<ISIRTemplateElement> TemplateElements { get; private set; }
        public IEnumerable<IEnumerable<ISIRCell>> IsirRows { get; set; }


        public ISIRTemplate()
        {
            TemplateElements = new List<ISIRTemplateElement>
            {
                new ISIRTemplateElement("1","Year Indicator", 1, 1, "numeric", "left"),
                new ISIRTemplateElement("2","Original SSN", 2, 9, "string", "right"),
                new ISIRTemplateElement("3","Original Name ID", 11, 2, "string", "left"),
                new ISIRTemplateElement("4","Transaction Number", 13, 2, "numeric", "right"),
                new ISIRTemplateElement("5","Last Name", 15, 16, "string", "left"),
                new ISIRTemplateElement("6","First Name", 31, 12, "string", "left"),
                new ISIRTemplateElement("7","Middle", 43, 1, "string", "left"),
                new ISIRTemplateElement("8","Mailing Address", 44, 35, "string", "left"),
                new ISIRTemplateElement("9","City", 79, 16, "string", "left"),
                new ISIRTemplateElement("10","State", 95, 2, "string", "left"),
                new ISIRTemplateElement("11","Zip Code", 97, 5, "string", "right"),
                new ISIRTemplateElement("12","DOB", 102, 8, "date", "left"),
                new ISIRTemplateElement("13","Phone Number", 110, 10, "string", "left"),
                new ISIRTemplateElement("16","Email Address", 142, 50, "string", "left"),
                new ISIRTemplateElement("33","High School Code", 304, 12, "string", "left"),
                new ISIRTemplateElement("35","Grade Level In College", 317, 1, "string", "left"),
                new ISIRTemplateElement("141","Fed School Code 1", 972, 6, "string", "left"),
                new ISIRTemplateElement("143","Fed School Code 2", 979, 6, "string", "left"),
                new ISIRTemplateElement("145","Fed School Code 3", 986, 6, "string", "left"),
                new ISIRTemplateElement("147","Fed School Code 4", 993, 6, "string", "left"),
                new ISIRTemplateElement("173","Transaction Data Soure/Type Code", 1139, 2, "string", "left"),
                new ISIRTemplateElement("174","Receipt Date", 1141, 8, "date", "left"),
                new ISIRTemplateElement("226","Current SSN", 1349, 9, "string", "left"),
				new ISIRTemplateElement("232","Application Receipt Date", 1377, 8, "date", "left"),
	            new ISIRTemplateElement("241","Source Of Correction", 1395, 1, "string", "left"),
				new ISIRTemplateElement("247","Reject Reason Code", 1408, 14, "string", "left"),
                new ISIRTemplateElement("288","Primary EFC", 1582, 6, "string", "left"),
                new ISIRTemplateElement("290","Signature Reject EFC", 1594, 6, "string", "left"),
                new ISIRTemplateElement("377","SSA Citizenship Flag", 2796, 1, "string", "left"),
                new ISIRTemplateElement("379","SSN Match Flag", 2805, 1, "string", "left"),
                new ISIRTemplateElement("384","Comment Code", 2866, 60, "string", "left")
            };

        }


        public ISIRTemplate(string workbookPath)
        {
            Application xlApplication = new Application();
            xlApplication.Visible = false;
            xlApplication.ShowStartupDialog = false;

            Workbook workbook = xlApplication.Workbooks.Open(workbookPath,Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

            Worksheet worksheet = workbook.Sheets["Sheet1"];

            ExtractData(worksheet);

            //Clean up
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //Release COM objects
            workbook.Close();
            Marshal.ReleaseComObject(workbook);

            xlApplication.Quit();
            Marshal.ReleaseComObject(xlApplication);


        }

        private void ExtractData(Worksheet worksheet)
        {
            List<ISIRTemplateElement> elements = new List<ISIRTemplateElement>();

            int rowcount = 0;

	        foreach (Range row in worksheet.Rows)
	        {

		        if (row.Value2[1,1] != null)
		        {
			        ISIRTemplateElement element = new ISIRTemplateElement();
			        foreach (Range cell in row.Cells)
			        {

				        if (cell.Column > 6)
				        {
					        break;
				        }

				        switch (cell.Column)
				        {
					        case 1:
						        element.RowNumber = cell.Text;
						        break;
					        case 2:
						        element.Start = (int) cell.Value2;
						        break;
					        case 3:
						        element.Length = (int) cell.Value2;
						        break;
					        case 4:
						        element.Field = cell.Text;
						        break;
					        case 5:
						        element.Type = cell.Text;
						        break;
					        case 6:
						        element.Justification = cell.Text;
						        break;
				        }
			        }

			        elements.Add(element);
			       
				}
		        else
		        {
			        break;

		        }
		        TemplateElements = elements;
			}

	       
        }

	               
        

    }



    
    public class ISIRTemplateElement
    {
        public string RowNumber { get; set; }
        public string Field { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }
        public string Justification { get; set; }

        public ISIRTemplateElement()
        {
        }

        public ISIRTemplateElement(string rownnumber,string field, int start, int length, string type, string justification)
        {
            RowNumber = rownnumber;
            Field = field;
            Start = start;
            Length = length;
            Type = type;
            Justification = justification;
        }
       
    
    }

    public class ISIRCell 
    {
        public string Value { get; set; }
        public ISIRTemplateElement Element { get; private set; }

        public ISIRCell(ISIRTemplateElement element, string value)
        {
            Element = element;
            Value = value;
        }
    }

    class ProcessingCodes
    {
        public string CodeToNumberConverter(string value)
        {
            var PCode = value.Substring(value.Length - 1, 1);

            switch (PCode)
            {
                case "{":
                    return value.Replace(PCode, "0");

                case "A":
                    return value.Replace(PCode, "1");

                case "B":
                    return value.Replace(PCode, "2");

                case "C":
                    return value.Replace(PCode, "3");

                case "D":
                    return value.Replace(PCode, "4");

                case "E":
                    return value.Replace(PCode, "5");

                case "F":
                    return value.Replace(PCode, "6");

                case "G":
                    return value.Replace(PCode, "7");

                case "H":
                    return value.Replace(PCode, "8");

                case "I":
                    return value.Replace(PCode, "9");

                case "}":
                    return "-" + value.Replace(PCode, "0");

                case "J":
                    return "-" + value.Replace(PCode, "1");

                case "K":
                    return "-" + value.Replace(PCode, "2");

                case "L":
                    return "-" + value.Replace(PCode, "3");

                case "M":
                    return "-" + value.Replace(PCode, "4");

                case "N":
                    return "-" + value.Replace(PCode, "5");

                case "O":
                    return "-" + value.Replace(PCode, "6");

                case "P":
                    return "-" + value.Replace(PCode, "7");

                case "Q":
                    return "-" + value.Replace(PCode, "8");

                case "R":
                    return "-" + value.Replace(PCode, "9");

                default:
                    return value;
            }
        }

        public string NumberToCodeConverter(string value)
        {
            var num = value.Substring(value.Length - 1, 1);
            var isSigned = (value.Substring(0, 1) == "-");
            var code = String.Empty;
            switch (num)
            {
                case "0":
                    if (!isSigned)
                    {
                        code = "{";
                        break;
                    }
                    else
                    {
                        code =  "}";
                        break;
                    }
                case "1":
                    if (!isSigned)
                    {
                        code = "A";
                        break;
                    }
                    else
                    {
                        code = "J";
                        break;
                    }
                case "2":
                    if (!isSigned)
                    {
                        code = "B";
                        break;
                    }
                    else
                    {
                        code = "K";
                        break;
                    }
                case "3":
                    if (!isSigned)
                    {
                        code = "C";
                        break;
                    }
                    else
                    {
                        code = "L";
                        break;
                    }
                case "4":
                    if (!isSigned)
                    {
                        code = "D";
                        break;
                    }
                    else
                    {
                        code = "M";
                        break;
                    }
                case "5":
                    if (!isSigned)
                    {
                        code = "E";
                        break;
                    }
                    else
                    {
                        code = "N";
                        break;
                    }
                case "6":
                    if (!isSigned)
                    {
                        code = "F";
                        break;
                    }
                    else
                    {
                        code = "O";
                        break;
                    }
                case "7":
                    if (!isSigned)
                    {
                        code = "G";
                        break;
                    }
                    else
                    {
                        code = "P";
                        break;
                    }
                case "8":
                    if (!isSigned)
                    {
                        code = "H";
                        break;
                    }
                    else
                    {
                        code = "Q";
                        break;
                    }
                case "9":
                    if (!isSigned)
                    {
                        code = "I";
                        break;
                    }
                    else
                    {
                        code = "R";
                        break;
                    }

                default:
                    return value;
            }

            value = value.Replace("-", string.Empty);
            return value.Remove(value.Length - 1, 1).Insert(value.Length - 1, code);
           

        }

        public string ISIRDateToDate(string value)
        {
            if (value.Trim().Length > 0)
            {
                var yy = value.Substring(0, 4);
                var mm = value.Substring(4, 2);
                var dd = value.Substring(6, 2);
                return mm + "/" + dd + "/" + yy;
            }
            return value;
        }

        public string ISIRShortDateToDate(string value)
        {
            if (value.Trim().Length > 0)
            {
                return $"{value.Substring(0, 4)}/{value.Substring(4, 2)}";

            }

            return value;
        }

        public string ShortDateToISIRShortDate(string value)
        {
            string[] parts = value.Split('/');
            return $"{parts[0]}{parts[1]}";
        }
        public string DateToISIRDate(string value)
        {
            string[] dateParts = value.Split('/');

            if (dateParts[0].Length != 2)
            {
                dateParts[0] = "0" + dateParts[0];
            }
            if (dateParts[1].Length != 2)
            {
                dateParts[1] = "0" + dateParts[1];
            }


            return dateParts[2] + dateParts[0] + dateParts[1];
        }
    }
}
