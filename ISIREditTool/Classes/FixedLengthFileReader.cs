﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using ISIREditTool.Classes;

namespace ISIREditTool.Classes
{
    class FixedLengthFileReader
    {
        private readonly string _filePath;
        private List<List<ISIRCell>> ISIRRows = new List<List<ISIRCell>>();

        public FixedLengthFileReader(string path)
        {
            _filePath = path;
        }

        public List<List<ISIRCell>> Read(DataGridView dg, ISIRTemplate template)
        {
            string line;
            StreamReader reader = new StreamReader(_filePath);
            ProcessingCodes pc = new ProcessingCodes();
            var FirstRow = true;
            while ((line = reader.ReadLine()) != null)
            {

                List<ISIRCell> ISIRCells = new List<ISIRCell>();
                if (!FirstRow)
                {
                    
                    DataGridViewRow row = new DataGridViewRow();
                    DataGridViewCell editCell = new DataGridViewButtonCell();
                    editCell.Value = "Edit";
                    row.Cells.Add(editCell);
                    foreach (ISIRTemplateElement element in template.TemplateElements)
                    {
                        var value = line.Substring(element.Start - 1, element.Length);
                        DataGridViewCell cell = new DataGridViewTextBoxCell();
                       
                        switch (element.Type)
                        {
                            case "money":
                                value = pc.CodeToNumberConverter(value);
                                break;

                            case "date":
                                value = pc.ISIRDateToDate(value);
                                break;

                            case "shortdate":
                                value = pc.ISIRShortDateToDate(value);
                                break;
                        }
                        cell.Value = value;
                        row.Cells.Add(cell);
                        cell.ReadOnly = true;
                        ISIRCells.Add(new ISIRCell(element, value));
                    }
                   
                    dg.Rows.Add(row);
                    ISIRRows.Add(ISIRCells);
                    Application.DoEvents();
                }
                else
                {
                    FirstRow = false;
                }
              
            }

            return ISIRRows;
        }
       

        }

   

    

}
