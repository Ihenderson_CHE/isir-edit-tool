﻿namespace ISIREditTool
{
    partial class frmEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel = new System.Windows.Forms.FlowLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cancelEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtBoxRowNumber = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.AutoScroll = true;
            this.panel.Location = new System.Drawing.Point(0, 30);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(890, 310);
            this.panel.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelEditToolStripMenuItem,
            this.saveEditToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.txtBoxRowNumber});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(895, 27);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cancelEditToolStripMenuItem
            // 
            this.cancelEditToolStripMenuItem.BackColor = System.Drawing.Color.LightSalmon;
            this.cancelEditToolStripMenuItem.Name = "cancelEditToolStripMenuItem";
            this.cancelEditToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 0, 10, 0);
            this.cancelEditToolStripMenuItem.Size = new System.Drawing.Size(84, 23);
            this.cancelEditToolStripMenuItem.Text = "Cancel Edit";
            this.cancelEditToolStripMenuItem.Click += new System.EventHandler(this.cancelEditToolStripMenuItem_Click);
            // 
            // saveEditToolStripMenuItem
            // 
            this.saveEditToolStripMenuItem.BackColor = System.Drawing.Color.MintCream;
            this.saveEditToolStripMenuItem.Name = "saveEditToolStripMenuItem";
            this.saveEditToolStripMenuItem.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.saveEditToolStripMenuItem.Size = new System.Drawing.Size(72, 23);
            this.saveEditToolStripMenuItem.Text = "Save Edit";
            this.saveEditToolStripMenuItem.Click += new System.EventHandler(this.saveEditsToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.BackColor = System.Drawing.Color.PapayaWhip;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(118, 23);
            this.searchToolStripMenuItem.Text = " Find Row Number";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.findRowNumber_Click);
            // 
            // txtBoxRowNumber
            // 
            this.txtBoxRowNumber.Name = "txtBoxRowNumber";
            this.txtBoxRowNumber.Size = new System.Drawing.Size(100, 23);
            this.txtBoxRowNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxRowNumber_KeyPress);
            // 
            // frmEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.AutoScrollMinSize = new System.Drawing.Size(5, 5);
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(895, 345);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit ISIR Data Row";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel panel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cancelEditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveEditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtBoxRowNumber;
    }
}