﻿namespace ISIREditTool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.grpBxSelectIsir = new System.Windows.Forms.GroupBox();
			this.lblProcessMessage = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnExport = new System.Windows.Forms.Button();
			this.lblFilePath = new System.Windows.Forms.Label();
			this.btnFileExplorer = new System.Windows.Forms.Button();
			this.dgrdISIR = new System.Windows.Forms.DataGridView();
			this.btnUpdateISIR = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblISIRRebuild = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lblFolderPath = new System.Windows.Forms.Label();
			this.btnFolderPath = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.grpBxFullTemplate = new System.Windows.Forms.GroupBox();
			this.lblPreparingTemplate = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btnLoadTemplate = new System.Windows.Forms.Button();
			this.lblISIRTemplatePath = new System.Windows.Forms.Label();
			this.btnTemplateExplorer = new System.Windows.Forms.Button();
			this.grpbxTemplate = new System.Windows.Forms.GroupBox();
			this.lblEdit = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.rbtnPartialTemplate = new System.Windows.Forms.RadioButton();
			this.rbtnFullTemplate = new System.Windows.Forms.RadioButton();
			this.grpBxSelectIsir.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgrdISIR)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.grpBxFullTemplate.SuspendLayout();
			this.grpbxTemplate.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpBxSelectIsir
			// 
			this.grpBxSelectIsir.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.grpBxSelectIsir.BackColor = System.Drawing.Color.AntiqueWhite;
			this.grpBxSelectIsir.Controls.Add(this.lblProcessMessage);
			this.grpBxSelectIsir.Controls.Add(this.label1);
			this.grpBxSelectIsir.Controls.Add(this.btnExport);
			this.grpBxSelectIsir.Controls.Add(this.lblFilePath);
			this.grpBxSelectIsir.Controls.Add(this.btnFileExplorer);
			this.grpBxSelectIsir.Location = new System.Drawing.Point(482, 52);
			this.grpBxSelectIsir.Name = "grpBxSelectIsir";
			this.grpBxSelectIsir.Size = new System.Drawing.Size(454, 143);
			this.grpBxSelectIsir.TabIndex = 7;
			this.grpBxSelectIsir.TabStop = false;
			this.grpBxSelectIsir.Visible = false;
			this.grpBxSelectIsir.VisibleChanged += new System.EventHandler(this.grpBxSelectIsir_VisibleChanged);
			// 
			// lblProcessMessage
			// 
			this.lblProcessMessage.AutoSize = true;
			this.lblProcessMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblProcessMessage.ForeColor = System.Drawing.Color.DarkGreen;
			this.lblProcessMessage.Location = new System.Drawing.Point(190, 117);
			this.lblProcessMessage.Name = "lblProcessMessage";
			this.lblProcessMessage.Size = new System.Drawing.Size(222, 17);
			this.lblProcessMessage.TabIndex = 10;
			this.lblProcessMessage.Text = "Processing ISIR Records.......";
			this.lblProcessMessage.Visible = false;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(226, 23);
			this.label1.TabIndex = 9;
			this.label1.Text = "Select ISIR File To Edit:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnExport
			// 
			this.btnExport.Enabled = false;
			this.btnExport.Location = new System.Drawing.Point(9, 111);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(161, 24);
			this.btnExport.TabIndex = 8;
			this.btnExport.Text = "Load ISIR Data";
			this.btnExport.UseVisualStyleBackColor = true;
			this.btnExport.EnabledChanged += new System.EventHandler(this.btnExport_EnabledChanged);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// lblFilePath
			// 
			this.lblFilePath.BackColor = System.Drawing.Color.WhiteSmoke;
			this.lblFilePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblFilePath.Location = new System.Drawing.Point(9, 69);
			this.lblFilePath.Name = "lblFilePath";
			this.lblFilePath.Size = new System.Drawing.Size(427, 39);
			this.lblFilePath.TabIndex = 7;
			this.lblFilePath.TextChanged += new System.EventHandler(this.lblFilePath_TextChanged);
			this.lblFilePath.Click += new System.EventHandler(this.lblFilePath_Click);
			// 
			// btnFileExplorer
			// 
			this.btnFileExplorer.Location = new System.Drawing.Point(9, 42);
			this.btnFileExplorer.Name = "btnFileExplorer";
			this.btnFileExplorer.Size = new System.Drawing.Size(161, 24);
			this.btnFileExplorer.TabIndex = 6;
			this.btnFileExplorer.Text = "Locate ISIR File To Edit";
			this.btnFileExplorer.UseVisualStyleBackColor = true;
			this.btnFileExplorer.Click += new System.EventHandler(this.btnFileExplorer_Click);
			// 
			// dgrdISIR
			// 
			this.dgrdISIR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dgrdISIR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgrdISIR.Location = new System.Drawing.Point(8, 201);
			this.dgrdISIR.Name = "dgrdISIR";
			this.dgrdISIR.Size = new System.Drawing.Size(1375, 505);
			this.dgrdISIR.TabIndex = 8;
			this.dgrdISIR.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrdISIR_CellContentClick);
			this.dgrdISIR.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrdISIR_CellValueChanged);
			// 
			// btnUpdateISIR
			// 
			this.btnUpdateISIR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnUpdateISIR.Location = new System.Drawing.Point(9, 111);
			this.btnUpdateISIR.Name = "btnUpdateISIR";
			this.btnUpdateISIR.Size = new System.Drawing.Size(161, 24);
			this.btnUpdateISIR.TabIndex = 9;
			this.btnUpdateISIR.Text = "Create ISIR With New Values";
			this.btnUpdateISIR.UseVisualStyleBackColor = true;
			this.btnUpdateISIR.EnabledChanged += new System.EventHandler(this.btnUpdateISIR_EnabledChanged);
			this.btnUpdateISIR.Click += new System.EventHandler(this.btnUpdateISIR_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.BackColor = System.Drawing.Color.AntiqueWhite;
			this.groupBox1.Controls.Add(this.lblISIRRebuild);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.btnUpdateISIR);
			this.groupBox1.Controls.Add(this.lblFolderPath);
			this.groupBox1.Controls.Add(this.btnFolderPath);
			this.groupBox1.Location = new System.Drawing.Point(958, 52);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(454, 143);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Visible = false;
			// 
			// lblISIRRebuild
			// 
			this.lblISIRRebuild.AutoSize = true;
			this.lblISIRRebuild.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblISIRRebuild.ForeColor = System.Drawing.Color.DarkGreen;
			this.lblISIRRebuild.Location = new System.Drawing.Point(176, 115);
			this.lblISIRRebuild.Name = "lblISIRRebuild";
			this.lblISIRRebuild.Size = new System.Drawing.Size(262, 17);
			this.lblISIRRebuild.TabIndex = 11;
			this.lblISIRRebuild.Text = "Rebuilding ISIR - Please be patient";
			this.lblISIRRebuild.Visible = false;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(6, 13);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(226, 23);
			this.label2.TabIndex = 9;
			this.label2.Text = "Select Location for new ISIR File:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.label2.Visible = false;
			// 
			// lblFolderPath
			// 
			this.lblFolderPath.BackColor = System.Drawing.Color.WhiteSmoke;
			this.lblFolderPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblFolderPath.Location = new System.Drawing.Point(9, 69);
			this.lblFolderPath.Name = "lblFolderPath";
			this.lblFolderPath.Size = new System.Drawing.Size(427, 39);
			this.lblFolderPath.TabIndex = 7;
			// 
			// btnFolderPath
			// 
			this.btnFolderPath.Location = new System.Drawing.Point(9, 42);
			this.btnFolderPath.Name = "btnFolderPath";
			this.btnFolderPath.Size = new System.Drawing.Size(161, 24);
			this.btnFolderPath.TabIndex = 6;
			this.btnFolderPath.Text = "Get location";
			this.btnFolderPath.UseVisualStyleBackColor = true;
			this.btnFolderPath.Click += new System.EventHandler(this.btnFolderPath_Click);
			// 
			// grpBxFullTemplate
			// 
			this.grpBxFullTemplate.BackColor = System.Drawing.Color.AntiqueWhite;
			this.grpBxFullTemplate.Controls.Add(this.lblPreparingTemplate);
			this.grpBxFullTemplate.Controls.Add(this.label4);
			this.grpBxFullTemplate.Controls.Add(this.btnLoadTemplate);
			this.grpBxFullTemplate.Controls.Add(this.lblISIRTemplatePath);
			this.grpBxFullTemplate.Controls.Add(this.btnTemplateExplorer);
			this.grpBxFullTemplate.Location = new System.Drawing.Point(8, 52);
			this.grpBxFullTemplate.Name = "grpBxFullTemplate";
			this.grpBxFullTemplate.Size = new System.Drawing.Size(454, 143);
			this.grpBxFullTemplate.TabIndex = 11;
			this.grpBxFullTemplate.TabStop = false;
			this.grpBxFullTemplate.Visible = false;
			// 
			// lblPreparingTemplate
			// 
			this.lblPreparingTemplate.AutoSize = true;
			this.lblPreparingTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPreparingTemplate.ForeColor = System.Drawing.Color.DarkGreen;
			this.lblPreparingTemplate.Location = new System.Drawing.Point(190, 117);
			this.lblPreparingTemplate.Name = "lblPreparingTemplate";
			this.lblPreparingTemplate.Size = new System.Drawing.Size(225, 17);
			this.lblPreparingTemplate.TabIndex = 10;
			this.lblPreparingTemplate.Text = "Preparing ISIR Template .......";
			this.lblPreparingTemplate.Visible = false;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(6, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(226, 23);
			this.label4.TabIndex = 9;
			this.label4.Text = "Select ISIR Template:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnLoadTemplate
			// 
			this.btnLoadTemplate.Enabled = false;
			this.btnLoadTemplate.Location = new System.Drawing.Point(9, 111);
			this.btnLoadTemplate.Name = "btnLoadTemplate";
			this.btnLoadTemplate.Size = new System.Drawing.Size(161, 24);
			this.btnLoadTemplate.TabIndex = 8;
			this.btnLoadTemplate.Text = "Prepare ISIR Template";
			this.btnLoadTemplate.UseVisualStyleBackColor = true;
			this.btnLoadTemplate.EnabledChanged += new System.EventHandler(this.btnLoadTemplate_EnabledChanged);
			this.btnLoadTemplate.Click += new System.EventHandler(this.btnLoadTemplate_Click);
			// 
			// lblISIRTemplatePath
			// 
			this.lblISIRTemplatePath.BackColor = System.Drawing.Color.WhiteSmoke;
			this.lblISIRTemplatePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblISIRTemplatePath.Location = new System.Drawing.Point(9, 69);
			this.lblISIRTemplatePath.Name = "lblISIRTemplatePath";
			this.lblISIRTemplatePath.Size = new System.Drawing.Size(427, 39);
			this.lblISIRTemplatePath.TabIndex = 7;
			// 
			// btnTemplateExplorer
			// 
			this.btnTemplateExplorer.Location = new System.Drawing.Point(9, 42);
			this.btnTemplateExplorer.Name = "btnTemplateExplorer";
			this.btnTemplateExplorer.Size = new System.Drawing.Size(161, 24);
			this.btnTemplateExplorer.TabIndex = 6;
			this.btnTemplateExplorer.Text = "Locate ISIR Template";
			this.btnTemplateExplorer.UseVisualStyleBackColor = true;
			this.btnTemplateExplorer.Click += new System.EventHandler(this.btnTemplateExplorer_Click);
			// 
			// grpbxTemplate
			// 
			this.grpbxTemplate.BackColor = System.Drawing.Color.AntiqueWhite;
			this.grpbxTemplate.Controls.Add(this.lblEdit);
			this.grpbxTemplate.Controls.Add(this.label6);
			this.grpbxTemplate.Controls.Add(this.rbtnPartialTemplate);
			this.grpbxTemplate.Controls.Add(this.rbtnFullTemplate);
			this.grpbxTemplate.Dock = System.Windows.Forms.DockStyle.Top;
			this.grpbxTemplate.Location = new System.Drawing.Point(0, 0);
			this.grpbxTemplate.Name = "grpbxTemplate";
			this.grpbxTemplate.Size = new System.Drawing.Size(1390, 46);
			this.grpbxTemplate.TabIndex = 12;
			this.grpbxTemplate.TabStop = false;
			// 
			// lblEdit
			// 
			this.lblEdit.AutoSize = true;
			this.lblEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblEdit.ForeColor = System.Drawing.Color.Green;
			this.lblEdit.Location = new System.Drawing.Point(733, 18);
			this.lblEdit.Name = "lblEdit";
			this.lblEdit.Size = new System.Drawing.Size(266, 16);
			this.lblEdit.TabIndex = 3;
			this.lblEdit.Text = "Extracting Row Data into Edit Form.....";
			this.lblEdit.Visible = false;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(30, 20);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(199, 13);
			this.label6.TabIndex = 2;
			this.label6.Text = "Select Template to use for editing";
			// 
			// rbtnPartialTemplate
			// 
			this.rbtnPartialTemplate.AutoSize = true;
			this.rbtnPartialTemplate.Location = new System.Drawing.Point(365, 18);
			this.rbtnPartialTemplate.Name = "rbtnPartialTemplate";
			this.rbtnPartialTemplate.Size = new System.Drawing.Size(125, 17);
			this.rbtnPartialTemplate.TabIndex = 1;
			this.rbtnPartialTemplate.TabStop = true;
			this.rbtnPartialTemplate.Text = "Partial ISIR Template";
			this.rbtnPartialTemplate.UseVisualStyleBackColor = true;
			this.rbtnPartialTemplate.CheckedChanged += new System.EventHandler(this.rbtnPartialTemplate_CheckedChanged);
			// 
			// rbtnFullTemplate
			// 
			this.rbtnFullTemplate.AutoSize = true;
			this.rbtnFullTemplate.Location = new System.Drawing.Point(247, 18);
			this.rbtnFullTemplate.Name = "rbtnFullTemplate";
			this.rbtnFullTemplate.Size = new System.Drawing.Size(112, 17);
			this.rbtnFullTemplate.TabIndex = 0;
			this.rbtnFullTemplate.TabStop = true;
			this.rbtnFullTemplate.Text = "Full ISIR Template";
			this.rbtnFullTemplate.UseVisualStyleBackColor = true;
			this.rbtnFullTemplate.CheckedChanged += new System.EventHandler(this.rbtnFullTemplate_CheckedChanged);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1390, 706);
			this.Controls.Add(this.grpbxTemplate);
			this.Controls.Add(this.grpBxFullTemplate);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.dgrdISIR);
			this.Controls.Add(this.grpBxSelectIsir);
			this.Name = "frmMain";
			this.Text = "ISIR Editing Tool";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.grpBxSelectIsir.ResumeLayout(false);
			this.grpBxSelectIsir.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgrdISIR)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.grpBxFullTemplate.ResumeLayout(false);
			this.grpBxFullTemplate.PerformLayout();
			this.grpbxTemplate.ResumeLayout(false);
			this.grpbxTemplate.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBxSelectIsir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.Button btnFileExplorer;
        private System.Windows.Forms.DataGridView dgrdISIR;
        private System.Windows.Forms.Button btnUpdateISIR;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFolderPath;
        private System.Windows.Forms.Button btnFolderPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label lblProcessMessage;
        private System.Windows.Forms.Label lblISIRRebuild;
        private System.Windows.Forms.GroupBox grpBxFullTemplate;
        private System.Windows.Forms.Label lblPreparingTemplate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLoadTemplate;
        private System.Windows.Forms.Label lblISIRTemplatePath;
        private System.Windows.Forms.Button btnTemplateExplorer;
        private System.Windows.Forms.GroupBox grpbxTemplate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rbtnPartialTemplate;
        private System.Windows.Forms.RadioButton rbtnFullTemplate;
        private System.Windows.Forms.Label lblEdit;
    }
}