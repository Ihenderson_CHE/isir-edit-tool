﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ISIREditTool.Classes;

namespace ISIREditTool
{
    public partial class mdiForm : Form
    {
        public  Classes.ISIR isir { get; set; }
        public mdiForm()
        {
            InitializeComponent();
        }

        private void mdiForm_Shown(object sender, EventArgs e)
        {
            frmISIRLocator frm = new frmISIRLocator
            {
                MdiParent = this,
                WindowState = FormWindowState.Maximized
            };
            frm.isir = new Classes.ISIR();
            frm.Show();
        }

        public void LoadISIRData()
        {
            ISIRElementList frm = new ISIRElementList();
            {
                MdiParent = this;
                WindowState = FormWindowState.Maximized;

            }
            frm.Show();


        }
    }
}
