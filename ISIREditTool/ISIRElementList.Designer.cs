﻿namespace ISIREditTool
{
    partial class ISIRElementList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrdISIR = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgrdISIR)).BeginInit();
            this.SuspendLayout();
            // 
            // dgrdISIR
            // 
            this.dgrdISIR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrdISIR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrdISIR.Location = new System.Drawing.Point(0, 0);
            this.dgrdISIR.Name = "dgrdISIR";
            this.dgrdISIR.Size = new System.Drawing.Size(1104, 593);
            this.dgrdISIR.TabIndex = 0;
            // 
            // ISIRElementList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 593);
            this.Controls.Add(this.dgrdISIR);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ISIRElementList";
            this.Text = "ISIRElementList";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ISIRElementList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrdISIR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrdISIR;
    }
}