﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISIREditTool
{
    public partial class frmISIRLocator : Form
    {
        public Classes.ISIR isir { get; set; }
        
        public frmISIRLocator()
        {
            InitializeComponent();
        }

        private void frmISIRLocator_ResizeEnd(object sender, EventArgs e)
        {
            grpBoxControls.Top = (this.Height - grpBoxControls.Height) / 2;
            grpBoxControls.Left = (this.Width - grpBoxControls.Width) / 2;
        }

        private void btnFileExplorer_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                lblFilePath.Text = dialog.FileName;
                isir.ISIRName = dialog.SafeFileName;
                if (dialog.SafeFileName != null) isir.ISIRPath = dialog.FileName.Replace(dialog.SafeFileName, "");

                btnExport.Enabled = true;
                btnFileExplorer.Enabled = false;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            mdiForm frm = (mdiForm)this.MdiParent;
            frm.isir = isir;
            frm.LoadISIRData();
            this.Close();
            Application.DoEvents();
        }
    }
}
