﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ISIREditTool.Classes;
using ISIREditTool.UserControls;

namespace ISIREditTool
{
    public partial class frmEdit : Form
    {
        private DataGridViewRow _row;
        private Int32 _rowIndex { get; set; }
        private DataGridView _dgv { get; set; }
        private List<List<ISIRCell>> _isir { get; set; }
        private ISIRTemplate _isirTemplate;

        public frmEdit(DataGridView view, DataGridViewRow row, Int32 rowIndex, ref List<List<ISIRCell>> ISIR, ISIRTemplate template)
        {
            InitializeComponent();
            _isirTemplate = template;
            _dgv = view;
            _row = row;
            _rowIndex = rowIndex;
            _isir = ISIR;
            LoadPanel(row);
            flowLayoutPanel1.WrapContents = false;
        }

        private void LoadPanel(DataGridViewRow row)
        {
            int i = 1;
            
            foreach (ISIRTemplateElement element in _isirTemplate.TemplateElements)
            {
                switch (element.Type)
                {
                    case "shortdate":
                        ucShortDate shortDate = new ucShortDate();
                        shortDate.RowNumber = element.RowNumber;
                        shortDate.LabelValue = element.Field;
                        try
                        {
                            shortDate.DateValue = row.Cells[i].Value.ToString();
                        }
                        catch (Exception e)
                        {
                            shortDate.DateValue = String.Empty;
                        }

                        shortDate.RowIndex = i;
                        panel.Controls.Add(shortDate);
                        break;

                    case "date":
                        UcDate ucDate = new UcDate();
                        ucDate.RowNumber = element.RowNumber;
                        ucDate.LabelValue = element.Field;
                        try
                        {
                            ucDate.DateValue = row.Cells[i].Value.ToString();
                        }
                        catch (Exception e)
                        {
                            ucDate.DateValue = String.Empty;
                        }
                       
                        ucDate.RowIndex = i;
                        panel.Controls.Add(ucDate);
                        break;

                    case "numeric":
                        UcNumeric ucNumeric = new UcNumeric();
                        ucNumeric.RowNumber = element.RowNumber;
                        ucNumeric.LabelValue = element.Field;
                        try
                        {
                            ucNumeric.NumericValue = row.Cells[i].Value.ToString();
                        }
                        catch (Exception e)
                        {
                            ucNumeric.NumericValue = String.Empty;
                        }
                        ucNumeric.RowIndex = i;
                        panel.Controls.Add(ucNumeric);
                        break;

                    //case "money":
                    //    UcText ucMoney = new UcText();
                    //    ucMoney.LabelValue = element.Field;
                    //    try
                    //    {
                    //        ucMoney.CurrencyValue = row.Cells[i].Value.ToString();
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        ucMoney.CurrencyValue = String.Empty;
                    //    }
                      
                    //    ucMoney.RowIndex = i;
                    //    panel.Controls.Add(ucMoney);
                    //    break;

                    default:
                        UcText ucText = new UcText();
                        ucText.LabelValue = element.Field;
                        ucText.RowNumber = element.RowNumber;
                        try
                        {
                            ucText.TextValue = row.Cells[i].Value.ToString();
                        }
                        catch (Exception e)
                        {
                            ucText.TextValue = String.Empty;
                        }
                        ucText.RowIndex = i;
                        panel.Controls.Add(ucText);
                        break;
   
                }

                i++;
            }
                             
        }

        private void cancelEditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void saveEditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReloadRow();
        }

        private void ReloadRow()
        {
            for (int i = 1; i < panel.Controls.Count;i++)
            {
                Type type = panel.Controls[i].GetType();
                if (type == typeof(UcNumeric))
                {
                    UcNumeric ucNumeric = (UcNumeric) panel.Controls[i];
                    _row.Cells[ucNumeric.RowIndex].Value = ucNumeric.NumericValue;
                   
                }

                if (type == typeof(UcDate))
                {
                    UcDate ucDate = (UcDate) panel.Controls[i];
                    _row.Cells[ucDate.RowIndex].Value = ucDate.DateValue;
                   
                }

                if (type == typeof(UcText))
                {
                    UcText ucText = (UcText) panel.Controls[i];
                    _row.Cells[ucText.RowIndex].Value = ucText.TextValue;
                }

                if (type == typeof(ucShortDate))
                {
                    ucShortDate shortDate = (ucShortDate) panel.Controls[i];
                    _row.Cells[shortDate.RowIndex].Value = shortDate.DateValue;
                }
            }

            if (_dgv.Rows.Count > _rowIndex)
            {
                for (int c = 1; c < _dgv.Rows[_rowIndex].Cells.Count;c++) 
                {
                    if (!Equals(_isir[_rowIndex][c-1].Value, _row.Cells[c].Value))
                    {
                        _dgv.Rows[_rowIndex].Cells[c].Value = _row.Cells[c].Value.ToString().Trim();
                        _isir[_rowIndex][c - 1].Value = _row.Cells[c].Value.ToString().Trim();
                        Application.DoEvents();
                    }

                }
                   
            }
            Close();
        }

        private void findRowNumber_Click(object sender, EventArgs e)
        {
            FindRow();
          
        }

        private void txtBoxRowNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals(Convert.ToChar(Keys.Enter)))
            {
                FindRow();
            }
        }

        private void FindRow()
        {
            for (int i = 0; i < panel.Controls.Count; i++)
            {
                Control control = panel.Controls[i];
                for (int c = 0; c < control.Controls.Count; c++)
                {
                    if (control.Controls[c].Name.Equals("lblRowNumber") &&
                        control.Controls[c].Text.Equals(txtBoxRowNumber.Text))
                    {

                        control.Focus();
                        return;
                    }

                }

            }
        }
    }
}
