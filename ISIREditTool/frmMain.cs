﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ISIREditTool.Classes;

namespace ISIREditTool
{
    public partial class frmMain : Form
    {
	    public string FilePath { get; set; }
	    private List<Int32> RowsEdited = new List<Int32>();
        private List<List<ISIRCell>> ISIR = new List<List<ISIRCell>>();
        private string fileName = String.Empty;
        private bool FullISIRTemplate { get; set; }
        private ISIRTemplate _isirTemplate;

        public frmMain()
        {
            InitializeComponent();
	        this.Text = $"{this.Text} v{GetVersion()}"; 
            this.WindowState= FormWindowState.Maximized;
           
        }

#region ISIR Selections

        private void rbtnFullTemplate_CheckedChanged(object sender, EventArgs e)
        {
            FullISIRTemplate = true;
            grpBxFullTemplate.Visible = false;
            grpBxFullTemplate.Top = 52;
            grpBxFullTemplate.Left = 8;
            grpBxFullTemplate.Visible = true;
	        dgrdISIR.Rows.Clear();
			dgrdISIR.Columns.Clear();
			dgrdISIR.Refresh();

			Application.DoEvents();
        }

        private void rbtnPartialTemplate_CheckedChanged(object sender, EventArgs e)
        {
            FullISIRTemplate = false;
            _isirTemplate = new ISIRTemplate();
            grpBxFullTemplate.Visible = false;
            grpBxSelectIsir.Top = 52;
            grpBxSelectIsir.Left = 8;
            grpBxSelectIsir.Visible = true;
			 PrepareTemplate();
            Application.DoEvents();
        }

#endregion

#region Full ISIR Actions

        private void btnTemplateExplorer_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.FileName = "*ISIRTemplate.xlsx";
            dialog.ShowHelp = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                lblISIRTemplatePath.Text = dialog.FileName;
                btnTemplateExplorer.Enabled = false;
                btnLoadTemplate.Enabled = true;

            }
        }

        private void btnLoadTemplate_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            lblPreparingTemplate.Visible = true;
            btnLoadTemplate.Enabled = false;
            Application.DoEvents();
            _isirTemplate = new ISIRTemplate(lblISIRTemplatePath.Text);
	        PrepareTemplate();
	        Application.DoEvents();
			grpBxFullTemplate.Visible = false;
            grpBxSelectIsir.Left = grpBxFullTemplate.Left + grpBxFullTemplate.Width + 10;
            grpBxSelectIsir.Top = 52;
            grpBxSelectIsir.Visible = true;
            Cursor.Current = Cursors.Default;
            Application.DoEvents();
        }

#endregion

#region ISIR File Extract

        private void btnFileExplorer_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = @"dat file (*.dat)|*.dat";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                lblFilePath.Text = dialog.FileName;
                fileName = dialog.SafeFileName;
	            btnFileExplorer.Enabled = false;
	            btnExport.Enabled = true;
			}
			
               
        }

	    private void btnExport_EnabledChanged(object sender, EventArgs e)
	    {
			Application.DoEvents();
		    if (btnExport.Enabled)
		    {
			    btnExport.Enabled = false;
				btnExport_Click(sender, e);
		    }
	    }

		private void btnExport_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            dgrdISIR.Enabled = false;
            lblProcessMessage.Visible = true;
         
            Application.DoEvents();
           
            LoadISIRRows();
            lblProcessMessage.Visible = false;
            dgrdISIR.Enabled = true;
            Cursor.Current = Cursors.Default;
            Application.DoEvents();
        }

#endregion

#region New ISIR Actions
        private void btnFolderPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string fullFilePath = dialog.SelectedPath + "/" + fileName;
                if (!File.Exists(fullFilePath))
                {
                    lblFolderPath.Text = dialog.SelectedPath;
					btnFolderPath.Enabled = false;
                }
                else
                {
                    MessageBox.Show(fullFilePath + @" already exists. Please choose a new location for ISIR File", @"ISIR Edit Tool",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        private void btnUpdateISIR_Click(object sender, EventArgs e)
        {

            btnUpdateISIR.Enabled = false;
            lblISIRRebuild.Visible = true;
            Application.DoEvents();

            FixedLengthFileWriter writer = new FixedLengthFileWriter(fileName, lblFolderPath.Text + "/" + fileName, ISIR);
            writer.Write();

            groupBox1.Visible = false;
            lblISIRRebuild.Visible = false;
            grpBxSelectIsir.Visible = true;
            btnFileExplorer.Enabled = true;
            dgrdISIR.Rows.Clear();
            dgrdISIR.Refresh();

            Application.DoEvents();
        }

#endregion


        private string GetVersion()
        {
            try
            {
				return $" - {System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()}";
			}
            catch (Exception)
            {
				return $" - {Assembly.GetExecutingAssembly().GetName().Version}";
			}
            
        }

       

        private void LoadISIRRows()
        {
            FixedLengthFileReader reader = new FixedLengthFileReader(lblFilePath.Text);
            ISIR = reader.Read(dgrdISIR, _isirTemplate);
            //btnUpdateISIR.Enabled = dgrdISIR.RowCount > 1;

        }

       
     

        private void EditRow(DataGridViewRow row, int rowIndex)
        {
            Cursor.Current = Cursors.WaitCursor;
            lblEdit.Visible = true;
            Application.DoEvents();
            frmEdit frm = new frmEdit(dgrdISIR,row, rowIndex, ref ISIR,_isirTemplate)
            {
                TopMost = true,
                WindowState = FormWindowState.Maximized
            };
            lblEdit.Visible = false;
            Cursor.Current = Cursors.Default;
            frm.Show();
        }

        private void dgrdISIR_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dgrdISIR.Rows[e.RowIndex];
            EditRow(row, e.RowIndex);
        }

        private void lblFilePath_TextChanged(object sender, EventArgs e)
        {
          
	        FilePath = lblFilePath.Text;
        }

        private void dgrdISIR_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!RowsEdited.Exists(element => element == e.RowIndex))
            {
                RowsEdited.Add(e.RowIndex);
                grpBxSelectIsir.Visible = false;
                groupBox1.Visible = true;
                Application.DoEvents();
            }
        }

       

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

       

      

        private void PrepareTemplate()
        {
            
            DataGridView view = this.dgrdISIR;
            view.Columns.Add(new DataGridViewButtonColumn());
            view.Columns[0].Frozen = true;
            foreach (ISIRTemplateElement element in _isirTemplate.TemplateElements)
            {
                view.Columns.Add(element.Field, element.Field);
                Application.DoEvents();
            }

           
        }

        private void grpBxSelectIsir_VisibleChanged(object sender, EventArgs e)
        {
            lblFilePath.Text = String.Empty;

        }

		private void lblFilePath_Click(object sender, EventArgs e)
		{

		}

		private void btnLoadTemplate_EnabledChanged(object sender, EventArgs e)
		{
			if (btnLoadTemplate.Enabled)
			{
				btnLoadTemplate_Click(sender, e);
			}
		}


		private void btnUpdateISIR_EnabledChanged(object sender, EventArgs e)
		{
			//	if (btnUpdateISIR.Enabled)
			//	{
			//		btnUpdateISIR_Click(sender, e);
			//	}
		}
	}
}
