﻿namespace ISIREditTool.UserControls
{
    partial class UcText
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.Label = new System.Windows.Forms.Label();
			this.Content = new System.Windows.Forms.TextBox();
			this.lblRowNumber = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.Location = new System.Drawing.Point(33, 12);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(176, 23);
			this.Label.TabIndex = 0;
			this.Label.Text = "label1";
			this.Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Content
			// 
			this.Content.Location = new System.Drawing.Point(216, 13);
			this.Content.Name = "Content";
			this.Content.Size = new System.Drawing.Size(241, 20);
			this.Content.TabIndex = 1;
			// 
			// lblRowNumber
			// 
			this.lblRowNumber.BackColor = System.Drawing.SystemColors.ControlDark;
			this.lblRowNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblRowNumber.Location = new System.Drawing.Point(3, 12);
			this.lblRowNumber.Name = "lblRowNumber";
			this.lblRowNumber.Size = new System.Drawing.Size(40, 23);
			this.lblRowNumber.TabIndex = 4;
			this.lblRowNumber.Text = "000";
			this.lblRowNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// UcText
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lblRowNumber);
			this.Controls.Add(this.Content);
			this.Controls.Add(this.Label);
			this.Name = "UcText";
			this.Size = new System.Drawing.Size(467, 46);
			this.Load += new System.EventHandler(this.ucText_Load);
			this.Enter += new System.EventHandler(this.ControlFocus);
			this.Leave += new System.EventHandler(this.ControlLostFocus);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label;
        private System.Windows.Forms.TextBox Content;
        private System.Windows.Forms.Label lblRowNumber;
    }
}
