﻿namespace ISIREditTool.UserControls
{
    partial class UcNumeric
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.Label = new System.Windows.Forms.Label();
			this.numericUpDown = new System.Windows.Forms.NumericUpDown();
			this.lblRowNumber = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.Location = new System.Drawing.Point(39, 10);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(334, 23);
			this.Label.TabIndex = 1;
			this.Label.Text = "label1";
			this.Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDown
			// 
			this.numericUpDown.Location = new System.Drawing.Point(379, 13);
			this.numericUpDown.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
			this.numericUpDown.Name = "numericUpDown";
			this.numericUpDown.Size = new System.Drawing.Size(76, 20);
			this.numericUpDown.TabIndex = 2;
			this.numericUpDown.Value = new decimal(new int[] {
            999999,
            0,
            0,
            0});
			// 
			// lblRowNumber
			// 
			this.lblRowNumber.BackColor = System.Drawing.SystemColors.ControlDark;
			this.lblRowNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblRowNumber.Location = new System.Drawing.Point(3, 12);
			this.lblRowNumber.Name = "lblRowNumber";
			this.lblRowNumber.Size = new System.Drawing.Size(40, 23);
			this.lblRowNumber.TabIndex = 4;
			this.lblRowNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// UcNumeric
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Controls.Add(this.lblRowNumber);
			this.Controls.Add(this.numericUpDown);
			this.Controls.Add(this.Label);
			this.Name = "UcNumeric";
			this.Size = new System.Drawing.Size(467, 46);
			this.Enter += new System.EventHandler(this.ControlFocus);
			this.Leave += new System.EventHandler(this.ControlLostFocus);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Label;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.Label lblRowNumber;
    }
}
