﻿namespace ISIREditTool.UserControls
{
    partial class UcDate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.Label = new System.Windows.Forms.Label();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.lblRowNumber = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.Location = new System.Drawing.Point(37, 12);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(221, 23);
			this.Label.TabIndex = 1;
			this.Label.Text = "label1";
			this.Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Location = new System.Drawing.Point(264, 15);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(190, 20);
			this.dateTimePicker.TabIndex = 2;
			this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
			// 
			// lblRowNumber
			// 
			this.lblRowNumber.BackColor = System.Drawing.SystemColors.ControlDark;
			this.lblRowNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblRowNumber.Location = new System.Drawing.Point(3, 12);
			this.lblRowNumber.Name = "lblRowNumber";
			this.lblRowNumber.Size = new System.Drawing.Size(40, 23);
			this.lblRowNumber.TabIndex = 3;
			this.lblRowNumber.Text = "000";
			this.lblRowNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// UcDate
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lblRowNumber);
			this.Controls.Add(this.dateTimePicker);
			this.Controls.Add(this.Label);
			this.Name = "UcDate";
			this.Size = new System.Drawing.Size(467, 46);
			this.Enter += new System.EventHandler(this.ControlFocus);
			this.Leave += new System.EventHandler(this.ControlLostFocus);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Label;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Label lblRowNumber;
    }
}
