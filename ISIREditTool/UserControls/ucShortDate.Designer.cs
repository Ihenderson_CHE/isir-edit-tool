﻿namespace ISIREditTool.UserControls
{
    partial class ucShortDate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.Label = new System.Windows.Forms.Label();
			this.numYear = new System.Windows.Forms.NumericUpDown();
			this.numMonth = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.lblRowNumber = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numMonth)).BeginInit();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.Location = new System.Drawing.Point(43, 12);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(300, 23);
			this.Label.TabIndex = 3;
			this.Label.Text = "label1";
			this.Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numYear
			// 
			this.numYear.Location = new System.Drawing.Point(402, 13);
			this.numYear.Maximum = new decimal(new int[] {
            2018,
            0,
            0,
            0});
			this.numYear.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
			this.numYear.Name = "numYear";
			this.numYear.Size = new System.Drawing.Size(54, 20);
			this.numYear.TabIndex = 4;
			this.numYear.Value = new decimal(new int[] {
            1900,
            0,
            0,
            0});
			// 
			// numMonth
			// 
			this.numMonth.Location = new System.Drawing.Point(349, 13);
			this.numMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
			this.numMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numMonth.Name = "numMonth";
			this.numMonth.Size = new System.Drawing.Size(44, 20);
			this.numMonth.TabIndex = 5;
			this.numMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(394, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(12, 18);
			this.label1.TabIndex = 6;
			this.label1.Text = "/";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblRowNumber
			// 
			this.lblRowNumber.BackColor = System.Drawing.SystemColors.ControlDark;
			this.lblRowNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblRowNumber.Location = new System.Drawing.Point(3, 12);
			this.lblRowNumber.Name = "lblRowNumber";
			this.lblRowNumber.Size = new System.Drawing.Size(40, 23);
			this.lblRowNumber.TabIndex = 7;
			this.lblRowNumber.Text = "000";
			this.lblRowNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ucShortDate
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lblRowNumber);
			this.Controls.Add(this.numMonth);
			this.Controls.Add(this.numYear);
			this.Controls.Add(this.Label);
			this.Controls.Add(this.label1);
			this.Name = "ucShortDate";
			this.Size = new System.Drawing.Size(467, 46);
			this.Enter += new System.EventHandler(this.ControlFocus);
			this.Leave += new System.EventHandler(this.ControlLostFocus);
			((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numMonth)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Label;
        private System.Windows.Forms.NumericUpDown numYear;
        private System.Windows.Forms.NumericUpDown numMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRowNumber;
    }
}
