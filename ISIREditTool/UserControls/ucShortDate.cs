﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISIREditTool.UserControls
{
    public partial class ucShortDate : UserControl
    {
        public int RowIndex{get; set; }
        public string RowNumber
        {
            get => lblRowNumber.Text;
            set => lblRowNumber.Text = value;
        }
        public string LabelValue
        {
            get => Label.Text;
            set => Label.Text = value;
        }

        public string DateValue
        {
            get => $"{numMonth}/{numYear}";
            set => SetDateValue(value);
        }

        public ucShortDate()
        {
            InitializeComponent();
        }

        private void SetDateValue(string date)
        {
            string[] parts = date.Split('/');
            if (parts.Count().Equals(2))
            {
                numMonth.Text = parts[0];
                numYear.Text = parts[1];
            }
        }
        private void ControlFocus(object sender, EventArgs e)
        {
            BackColor = Color.AntiqueWhite;
            BorderStyle = BorderStyle.Fixed3D;
        }

        private void ControlLostFocus(object sender, EventArgs e)
        {
            BackColor = Color.Empty;
            BorderStyle = BorderStyle.None;
        }
    }
}
