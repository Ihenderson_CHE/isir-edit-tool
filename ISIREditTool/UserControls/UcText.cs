﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ISIREditTool.UserControls
{
    public partial class UcText : UserControl
    {
        public int RowIndex { get; set; }

        public string RowNumber
        {
            get => lblRowNumber.Text;
            set => lblRowNumber.Text = value;
        }

        public string LabelValue
        {
            get => Label.Text;
            set => Label.Text = value;
        }

        public string TextValue
        {
            get => Content.Text;
            set => Content.Text = value.Trim();
        }

        public string CurrencyValue
        {
            set => SetCurrency(value);
        }

        public UcText()
        {
            InitializeComponent();
        }

        private void ucText_Load(object sender, EventArgs e)
        {

        }

        private void SetCurrency(string value)
        {
            Content.Text = $"{value:C}";

        }
        private void ControlFocus(object sender, EventArgs e)
        {
            BackColor = Color.AntiqueWhite;
            BorderStyle = BorderStyle.Fixed3D;
        }

        private void ControlLostFocus(object sender, EventArgs e)
        {
            BackColor = Color.Empty;
            BorderStyle = BorderStyle.None;
        }
    }
}
