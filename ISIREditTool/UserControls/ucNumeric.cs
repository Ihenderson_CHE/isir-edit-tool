﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISIREditTool.UserControls
{
    public partial class UcNumeric : UserControl
    {
        public int RowIndex { get; set; }
        public string RowNumber
        {
            get => lblRowNumber.Text;
            set => lblRowNumber.Text = value;
        }
        public string LabelValue
        {
            get { return Label.Text; }
            set { Label.Text = value; }

        }

        public string NumericValue
        {
            get { return numericUpDown.Text; }
            set { SetNumericValue(value.Trim()); }
        }

        public UcNumeric()
        {
            InitializeComponent();
        }

        private void SetNumericValue(string value)
        {
            try
            {
                int val = Convert.ToInt16(value);
                numericUpDown.Value = val;
            }
            catch(Exception)
            {
                numericUpDown.Value = 0;
            }

        }
        private void ControlFocus(object sender, EventArgs e)
        {
            BackColor = Color.AntiqueWhite;
            BorderStyle = BorderStyle.Fixed3D;
        }

        private void ControlLostFocus(object sender, EventArgs e)
        {
            BackColor = Color.Empty;
            BorderStyle = BorderStyle.None;
        }
    }
}
