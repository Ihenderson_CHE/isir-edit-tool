﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISIREditTool.UserControls
{
    public partial class UcDate : UserControl
    {
        public int RowIndex { get; set; }

        public string RowNumber
        {
            get => lblRowNumber.Text;
            set => lblRowNumber.Text = value;
        }

        public string LabelValue
        {
            get => Label.Text;
            set => Label.Text = value;
        }

        public string DateValue
        {
            get => dateTimePicker.Value.ToShortDateString();
            set => SetDateValue(value);
        }

       

        public UcDate()
        {
            InitializeComponent();
        }

        private void SetDateValue(string date)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(date);
                dateTimePicker.Value = dt;
            }
            catch (Exception)
            {
                dateTimePicker.Value = Convert.ToDateTime("01/01/1900");
            }
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ControlFocus(object sender, EventArgs e)
        {
            BackColor= Color.AntiqueWhite;
            BorderStyle = BorderStyle.Fixed3D;
        }

        private void ControlLostFocus(object sender, EventArgs e)
        {
            BackColor = Color.Empty;
            BorderStyle = BorderStyle.None;
        }
    }
}
